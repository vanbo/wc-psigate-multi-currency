<?php
/*
 * Plugin Name: Psigate multi-currency
 * Plugin URI: http://www.vanbodevelops.com/product/https:/woocommerce-psigate-multi-currency
 * Description: Allows you to use <a href="http://www.psigate.com/">PsiGate</a> payment processor with the WooCommerce plugin.
 * Version: 1.0.0
 * Author: VanboDevelops
 * Author URI: http://www.vanbodevelops.com
 * WC requires at least: 3.0.0
 * WC tested up to: 5.1.0
 *
 *	Copyright: (c) 2014 - 2020 VanboDevelops | Ivan Andreev
 *	License: GNU General Public License v3.0
 *	License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_Psigate_MC {
	
	const VERSION = '1.0.0';
	/**
	 * The files and folders version
	 * Should be changed every time there is a new class file added or one deleted
	 */
	const FILES_VERSION = '1.1.1';
	const MIN_PHP_VERSION = '5.6.0';
	const MIN_WC_VERSION = '3.0.0';
	/**
	 * Plugin path
	 * @var string
	 */
	public $plugin_dir_path;
	/**
	 * Plugin url
	 * @var string
	 */
	public $plugin_url;
	/**
	 * Text domain string. Constant
	 */
	const TEXT_DOMAIN = 'wc_psigate_multi_currency';
	/**
	 * @var \WC_Psigate_MC
	 */
	protected static $_instance = null;
	
	/**
	 * @return \WC_Psigate_MC|null
	 * @throws \Exception
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	public function __clone() {
		wc_doing_it_wrong( __FUNCTION__, __( 'Nope. No Cloning', 'wc_psigate_multi_currency' ), '1.2.0' );
	}
	
	public function __wakeup() {
		wc_doing_it_wrong( __FUNCTION__, __( 'Nope! Not this route!', 'wc_psigate_multi_currency' ), '1.2.0' );
	}
	
	/**
	 * WC_Psigate_MC constructor.
	 * @throws Exception
	 */
	public function __construct() {
		$this->load_hooks();
	}
	
	/**
	 * @since 1.2.0
	 */
	public function load_hooks() {
		add_filter( 'woocommerce_settings_api_form_fields_psigate', array(
			$this,
			'wc_multicurrency_settings_setup',
		), 10 );
		
		// AM API Process Payment
		add_filter( 'wc_psigate_process_acc_manager_payment_parameters', array(
			$this,
			'psigate_modify_account_manager_api_credentials_on_currency',
		), 10, 2 );
		// AM API Register Customer Account
		add_filter( 'wc_psigate_register_customer_account_parameters', array(
			$this,
			'psigate_modify_account_manager_api_credentials_on_currency',
		), 10, 2 );
		// XML API Process Payment
		add_filter( 'wc_psigate_process_credit_card_payment_parameters', array(
			$this,
			'psigate_modify_xml_api_credentials_on_currency',
		), 10, 2 );
		// XML API Process Refund
		add_filter( 'wc_psigate_process_refund_parameters', array(
			$this,
			'psigate_modify_xml_api_credentials_on_currency',
		), 10, 2 );
		// AM Create account from Payment
		add_filter( 'wc_psigate_create_am_account_from_payment_parameters', array(
			$this,
			'psigate_create_account_from_payment_on_currency',
		), 10, 3 );
	}
	
	public function wc_multicurrency_settings_setup( $settings ) {
		
		$my_settings = array();
		
		$alt_currency_value = $this->get_alt_currency();
		
		
		foreach ( $settings as $key => $setting ) {
			$my_settings[ $key ] = $setting;
			
			if ( 'xml_api_end' == $key ) {
				$my_settings['xml_api_settings_alt_currency'] = array(
					'title'       => __( 'XML API Settings for ' . $alt_currency_value . 'currency.', 'wc_psigate_multi_currency' ),
					'type'        => 'title',
					'description' => '',
					'desc_tip'    => true,
					'class'       => '',
				);
				$my_settings['StoreID_alt_currency']          = array(
					'title'       => __( 'Store ID for ' . $alt_currency_value . ' currency.', 'wc_psigate_multi_currency' ),
					'type'        => 'text',
					'description' => __( 'Enter your StoreID received from PsiGate for ' . $alt_currency_value . ' currency.', 'wc_psigate_multi_currency' ),
					'default'     => '',
				);
				$my_settings['PassPhrase_alt_currency']       = array(
					'title'       => __( 'Pass Phrase for ' . $alt_currency_value . ' currency', 'wc_psigate_multi_currency' ),
					'type'        => 'password',
					'description' => __( 'Enter your Passphrase received from PsiGate for ' . $alt_currency_value . ' currency. The field will always appear empty.', 'wc_psigate_multi_currency' ),
					'default'     => '',
				);
				
				$my_settings['xml_api_end_alt_currency'] = array(
					'title'       => '<hr/>',
					'type'        => 'title',
					'description' => '',
					'class'       => '',
				);
			}
		}
		
		return $my_settings;
	}
	
	/**
	 * Modifies the Account Manager API credentials, based on currency code
	 *
	 * @param array    $parameters
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function psigate_modify_account_manager_api_credentials_on_currency( $parameters, $order ) {
		
		/**
		 * IMPORTANT: For the default currency the setting "Store ID" in the plugin settings is used.
		 *            You can still overwrite it here by adding the default currency
		 */
		
		$gateway = \WcPsigate\Helpers\Factories::get_gateway( 'psigate' );
		if ( $this->get_alt_currency() == \WcPsigate\Compatibility::get_order_currency( $order ) ) {
			if ( isset( $parameters['Charge'] ) && isset( $parameters['Charge']['StoreID'] ) ) {
				$parameters['Charge']['StoreID'] = $gateway->get_option( 'StoreID_alt_currency' );
			}
		}
		
		return $parameters;
	}
	
	/**
	 * Returns the alternative currency
	 * @return string
	 */
	public function get_alt_currency() {
		$store_currency = get_woocommerce_currency();
		
		if ( 'USD' === $store_currency ) {
			$alt_currency_value = 'CAD';
		} else {
			$alt_currency_value = 'USD';
		}
		
		return $alt_currency_value;
	}
	
	/**
	 * Modifies the XML API credentials, based on currency code
	 *
	 * @param array    $parameters
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	public function psigate_modify_xml_api_credentials_on_currency( $parameters, $order ) {
		
		$gateway = \WcPsigate\Helpers\Factories::get_gateway( 'psigate' );
		if ( $this->get_alt_currency() == \WcPsigate\Compatibility::get_order_currency( $order ) ) {
			/**
			 *  Important: Credentials for the XML API, based on the currency
			 */
			if ( isset( $parameters['StoreID'] ) ) {
				$parameters['StoreID'] = $gateway->get_option( 'StoreID_alt_currency' );
			}
			if ( isset( $parameters['Passphrase'] ) ) {
				$parameters['Passphrase'] = $gateway->get_option( 'PassPhrase_alt_currency' );
			}
		}
		
		return $parameters;
	}
	
	/**
	 * Modifies the AM API parameters for Create account from payment request
	 *
	 * @param array    $parameters
	 * @param WC_Order $order
	 *
	 * @return array
	 */
	function psigate_create_account_from_payment_on_currency( $parameters, $order, $payment_response ) {
		// Enter here the currency code you want to have additional account for.
		
		
		$gateway = \WcPsigate\Helpers\Factories::get_gateway( 'psigate' );
		if ( $this->get_alt_currency() == \WcPsigate\Compatibility::get_order_currency( $order ) ) {
			
			if ( isset( $parameters['Condition'] ) && isset( $parameters['Condition']['StoreID'] ) ) {
				$parameters['Condition']['StoreID'] = $gateway->get_option( 'StoreID_alt_currency' );
			}
		}
		
		return $parameters;
	}
	
	/**
	 * Load Text domains
	 * @since 1.0.0
	 */
	public function load_text_domain() {
		// Add localization on init so WPML to be able to use it.
		load_textdomain( 'wc_psigate_multi_currency', WP_LANG_DIR . '/wc-psigateway-multi-currency/wc_psi_gate_mc-' . get_locale() . '.mo' );
		load_plugin_textdomain( 'wc_psi_gateway_mc', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}
	
	/**
	 * Get plugin url
	 *
	 * @since 1.1
	 * @return string
	 */
	public function get_plugin_url() {
		
		if ( $this->plugin_url ) {
			return $this->plugin_url;
		}
		
		return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}
	
	/**
	 * Get plugin path
	 *
	 * @since 1.1
	 * @return string
	 */
	public function get_plugin_path() {
		
		if ( $this->plugin_dir_path ) {
			return $this->plugin_dir_path;
		}
		
		return $this->plugin_dir_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}
	
	/**
	 * Safely retrieve an array or object key/property
	 *
	 * @since 1.2.0
	 *
	 * @param string       $name  Name of the key/prop
	 * @param array|object $stack The stack we are looking in
	 * @param string       $default
	 *
	 * @return mixed The variable value
	 */
	public function get_field( $name, $stack, $default = '' ) {
		
		if ( is_array( $stack ) ) {
			if ( isset( $stack[ $name ] ) ) {
				return $stack[ $name ];
			}
		}
		
		if ( is_object( $stack ) ) {
			if ( isset( $stack->{$name} ) ) {
				return $stack->{$name};
			}
		}
		
		return $default;
	}
}

add_action( 'plugins_loaded', 'wc_psi_gate_mc' );
/**
 * @return \WC_Psigate_MC|null
 * @throws \Exception
 */
function wc_psi_gate_mc() {
	try {
		return WC_Psigate_MC::instance();
	}
	catch ( Exception $e ) {
		$message = 'Plugin was not loaded. Error message: ' . $e->getMessage();
		throw new \Exception( $message );
	}
}